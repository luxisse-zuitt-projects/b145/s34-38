const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const Course = require('../models/Course');



// Check if email exists
module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return true
		} else {
			return false
		}
	})
};



// Registration
module.exports.registerUser = (reqBody) => {
	
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, err) => {

		if(err){
			
			return false
		
		} else {
			
			return user
		
		}
	})
}



// Login
module.exports.loginUser = (reqBody) => {

	return User.findOne({email : reqBody.email}).then(result => {
		
		if(result == null){

			return false
		
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			
			if(isPasswordCorrect){
					return { access : auth.createAccessToken(result)}
			} else {
					return false
			};
		};
	});
};



// START OF API PART 2 ACTIVITY

// Retrieve User Details
// module.exports.getProfile = (userId) => {

// 	return User.findById(userId).then((result, err) => {
// 		if(err){
// 			console.log(err);
// 			return err;
		
// 		} else {

// 		// Reassign password to empty string
// 		result.password = " ";

// 			return result.save().then((updatedPassword, saveErr) => {
// 				if (saveErr) {
// 					console.log(saveErr);
// 					return saveErr;

// 				} else {
// 					return updatedPassword;
// 				};
// 			});
// 		};
// 	});
// };



/*
	API PART 2 A C T I V I T Y S O L U T I O N

*/

module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});
};




// START OF API PART 5 ACTIVITY
// // Enroll regular user with authentication

module.exports.enroll = async (data) => {
	
	if(data.payload.isAdmin !== false) {
		return `Forbidden action`
	}

	let isUserUpdated = await User.findById(data.payload.id).then(user => {

		user.enrollments.push({courseId : data.courseId})
		
		return user.save().then((user, err) => {
			
			if(err){
				 	
				 	return false;
			
			} else {
					
					return true;
			
			};
		});
	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

		course.enrollees.push({userId : data.payload.id})

		return course.save().then((course, err) => {
			
			if(err){
				
				return false;
			
			} else {
				
				return true;
			
			};
		});
	});

	if(isUserUpdated && isCourseUpdated){
		
			return `Enrolled successfully`;
	
	} else {

			return `Try again`;

	};
};