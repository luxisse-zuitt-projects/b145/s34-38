const Course = require('../models/Course');
const auth = require('../auth');

// Add a course

module.exports.addCourse = (reqBody, isAdmin) => {

	if(isAdmin) {

		let newCourse = new Course({
		 	name : reqBody.name,
		 	description : reqBody.description,
		 	price : reqBody.price
		});

		return newCourse.save().then((user, err) => {
		
			if(err){
					return false;
			} else {
					return true;
			}

		});
	}
	return Promise.resolve('Unauthorized request');
};



// Retrieving all courses

module.exports.getAllCourses = async (user) => {
	if(user.isAdmin === true){
		return Course.find({}).then(result => {
				return result;
		})
	} else {
				return `${user.email} is not authorized`;
	};
};



// Retrieval of active courses

module.exports.getAllActive = () => {
	
	return Course.find({isActive : true}).then(result => {
		
		return result;

	});
};



// Retrieval of a specific course

module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {
		return result;
	});

};



// Updating a course
module.exports.updatedCourse = (data) => {

	console.log(data);
	return Course.findById(data.courseId).then((result, err) => {
		console.log(result)
		if(data.payload.isAdmin === true){
			
				result.name = data.updatedCourse.name
				result.description = data.updatedCourse.description
				result.price = data.updatedCourse.price
			
			console.log(result);
			
			return result.save().then((updatedCourse, err) => {
				console.log(updatedCourse)
				if(err){
						return false;
				} else {
						return updatedCourse;
				}

			});

		}	else {
				return false;
		}

	});
};



// Archive course
module.exports.archivedCourse = async (data) => {
	
	console.log(data);

	if(data.payload.isAdmin === true) {

		return Course.findById(data.courseId).then((result, err) => {
			result.isActive = false;

			return result.save().then((archivedCourse, err) => {
				if(err) {
					return false;
				}
				else {
					return result;
				}
			})
		})
	} else {
		return 'Not permitted to do this action';
	};

};